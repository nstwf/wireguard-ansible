### Ansible wireguard client/server setup

* Configure clients and servers
* Share configs

### How to use

```shell
cp inventory.example.yaml inventory.yaml

ansible-playbook playbook.yml -i inventory.yaml
```

### Linting:

```shell
ansible-lint --write all
```
